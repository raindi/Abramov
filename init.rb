# run this file to run Abramov app

APP_ROOT = File.dirname(__FILE__) # setting app root
$LOAD_PATH.unshift(File.join(APP_ROOT, 'engine'))
require 'app'

app = App.new
app.run!
