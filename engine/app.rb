#require 'colorize'
require 'yaml'
require 'abramov_tasks'

class App
  include Abramov

  def initialize
    @separator = '-' * 60
    @cursor = '> '
    @details = YAML.load(File.open('engine/abramov_tasks.yml'))
    @descriptions = @details[:descriptions]
    @comands = @details[:comands]
    @executed_task = nil
    @details_showed = false
  end

  def show_descriptions
    # shows list of descriptions to each task
    puts @separator
    @descriptions.each { |descr| puts descr }
    puts @separator
  end

  def show_comands
    # shows list of comands
    @comands.each { |descr| puts descr }
    puts @separator
  end

  def show_code(task)
    # prints to console method code for a given task
    show = false
    endofcode = ''
    code = IO.read('engine/abramov_tasks.rb').lines
    code.each do |line|
      if line.include?("def ex#{task}")
        show = true
        endofcode = (' ' * (line.length - line.strip.length - 1)) + "end\n"
      end
      print line.yellow if show
      next unless line == endofcode
      show = false
      endofcode = ''
      puts @separator
    end
  end

  def interact
    # interaction wth user
    loop do
      unless @executed_task.nil? # executing chosen task
        unless @details_showed
          puts "Author: #{@details[:author][@executed_task.name]}"
          @details_showed = true
        end
        prm = ''
        @executed_task.parameters.each { |e| prm += e[1].to_s + ' ' if e[0] == :req }
        puts "Enter #{prm}"
        print @cursor
        prm = gets.chomp.split(' ')
        prm.map!(&:to_i)
        puts @executed_task.call(*prm).to_s#.green
        puts 'Execute selected task one more time? [Y/n]'
        print @cursor
        if gets.chomp == 'n' # back to task choise
          @executed_task = nil
          @details_showed = false
        end
        next
      end
      puts 'Enter task number' # chosing task to execute
      print @cursor
      input = gets.chomp.downcase
      @executed_task = method("ex#{input}") if respond_to?("ex#{input}")
      show_descriptions if input == 'tasks'
      show_comands if input == 'comands'
      show_code(input.slice(5, input.length)) if input.include? 'code'
      break if input == 'exit'
    end
  end

  def run!
    # runnig interaction with user
    show_descriptions
    show_comands
    interact
  end
end
