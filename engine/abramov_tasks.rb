# this module contents methods for solving exercises from Abramov
module Abramov
  def ex330(number)
    result_arr = []
    (1..number).each do |n| # Iterating through numbers from 1 to number
      arr = []
      (1...n).each do |num|
        arr << num if n % num == 0 # Push all proper positive divisors to arr
      end
      sum = 0
      arr.map { |x| sum += x } # Find sum of proper positive divisors in arr
      result_arr << sum if n == sum # If sum == n then push sum to result_arr
    end
    result_arr
  end

  def ex86a(number)
    number.to_s.size
  end

  def ex86b(number)
    result = 0
    number.to_s.split('').map { |x| result += x.to_i }
    result
  end

  def ex87(digets, number)
    if digets < number
      puts 'E: First element must be greater then the second. Pleace try agen'
    else
      a = digets.to_s.split('').map(&:to_i)
      a[-number, number].inject(0, :+)
    end
  end

  def ex88c(number)
    # swaps first and last digits in the number
    number = number.to_s
    sign = ''
    if number[0] == '-'
      sign = number.slice(0, 1)
      number = number.slice(1, number.length)
    end
    number[0], number[-1] = number[-1], number[0]
    (sign + number).to_i
  end

  def ex88d(number)
    # adds 1 in front and to back of the number
    number = number.to_s
    sign = ''
    if number[0] == '-'
      sign = number.slice(0, 1)
      number = number.slice(1, a.length)
    end
    (sign + '1' + number + '1').to_i
  end

  def ex226(first, second)
    a = [first, second]
    (a.min...a.inject(1, :*)).find_all { |num| num % first == 0 && num % second == 0 }
  end

  def ex332(number, res = [])
    # returns array of four or less numbers which squares sum is equal to the given number
    i = Math.sqrt(number).floor
    res << i
    tmp = number - i**2
    ex332(tmp, res) if tmp != 0 && res.length < 4 # recursion
    # if this algoritm fails to find a valid sequence, get it by brut force
    res = ex332_get_seq(ex332_sq_sum(res)) if res.length > 4
    res
  end

  def ex559(n)
    (0..n).map { |n| 2**n - 1 }
  end

  # 107 There is integer m>1. Return bigger integer k, when 4^k<m
  def ex107(number)
    degree = 0
    degree += 1 while number > 4**degree
    puts "The biggest k , when 4 ^ k < #{number} is #{degree - 1}"
    degree - 1
  end

  # x^2 + y^2 = n. find pair where x^2 + y^2 = n
  def ex243a(number)
    x = y = 1
    while x < number
      square_y = number - x * x # y^2 = number - x^2
      if square_y > 0
        y = Math.sqrt(square_y)
        return "#{x} #{y.round}" if y - y.round == 0
      end
      x += 1
    end
  end

  # x^2 + y^2 = n. Find all pair x and y where x >= y
  def ex243b(number)
    x = y = 1
    results = {}
    while x < number
      square_y = number - x * x # y^2 = n - x^2
      if square_y > 0
        y = Math.sqrt(square_y)
        if y - y.round == 0 && x >= y
          results[x] = y.round # if x >= y
        end
      end
      x += 1
    end
    results
  end

  def ex178a(*arr)
    arr.select(&:odd?).length
  end

  def ex178b(*arr)
    arr.select { |x| x % 3 == 0 && x % 5 != 0 }.length
  end

  def ex178c(*arr)
    k = 0
    arr.each_cons(3) do |a_pr, a_cur, a_next|
      k += 1 if a_cur < (a_pr + a_next) / 2.0
    end
    k
  end

  def ex554(n)
    s = ''
    1.upto(n) do |c|
      1.upto(c) do |b|
        1.upto(b) do |a|
          s += "(#{a},#{b},#{c}) \n" if a * a + b * b == c * c
        end
      end
    end
    s
  end

  def ex321(number, x = 0)
    while x < number
      y = x - 1
      x += 1
      while y < number
        z = y - 1
        y += 1
        while z < number
          z += 1
          if (x**2 + y**2 + z**2) == number
            puts "x: #{x}  y: #{y}  z: #{z}"
            next
          end
        end
      end
    end
  end

  def ex118(number, root = 1)
    root *= 2 until root > number
    puts "The closest number is: #{root}"
    end

  def ex88b(number, changer = 0)
    while number > 0 do
      changer = number * 10 + number % 10
      number /= 10
    end
    puts "Result is: #{changer}"
  end
  
  private

  def ex332_get_seq(num)
    # used when there is only 1 sequence that can be used as an answer
    # and lagrange recurent method didnt come up with it (numbers 23,32...)
    max = Math.sqrt(num).floor
    fq = [0, 0, 0, 0]
    loop do
      ex332_inter(fq, max)
      break if fq[3] > max
      break if ex332_sq_sum(fq) == num
      fq[3] += 1
    end
    fq.reverse
  end

  def ex332_inter(arr, max)
    # a single interation for brut force lagrange
    if arr[3] > max
      arr[2] += 1
      arr[3] = 0
    elsif arr[2] > max
      arr[1] += 1
      arr[2] = 0
    elsif arr[1] > max
      arr[0] += 1
      arr[1] = 0
    end
  end

  def ex332_sq_sum(arr)
    # returns square sum of numbers in given array
    # used for getting lagrange sequence
    arr.inject { |a, e| a + e**2 }
  end
end
